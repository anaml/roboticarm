import cv2 as cv
from cv2 import aruco
import numpy as np
from pypylon import pylon
from PIL import Image
import pyglet
from pyglet.gl import *
from pipython import GCSDevice

event_loop = pyglet.app.EventLoop()

pidevice = GCSDevice()
pidevice.ConnectUSB(serialnum="120060503")
print('connected: {:s}'.format(pidevice.qIDN().strip()))

pidevice.MOV(2, 0)
pidevice.MOV(1, 2)
camera = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateFirstDevice())
camera.Open()
print(cv.__version__)

# load in the calibration data
calib_data_path = "MultiMatrix.npz"

calib_data = np.load(calib_data_path)
print(calib_data.files)

cam_mat = calib_data["camMatrix"]
dist_coef = calib_data["distCoef"]
r_vectors = calib_data["rVector"]
t_vectors = calib_data["tVector"]

MARKER_SIZE = 1  # centimeters (measure your printed marker size)

marker_dict = aruco.getPredefinedDictionary(aruco.DICT_5X5_250)
param_markers = aruco.DetectorParameters()
detector = aruco.ArucoDetector(marker_dict, param_markers)

# cap = cv.VideoCapture(0)

center_point_id0 = (0, 0)
center_point_id1 = (0, 0)


def draw_grid(img, grid_shape, color=(0, 255, 0), thickness=1):
    h, w, _ = img.shape
    rows, cols = grid_shape
    dy, dx = h / rows, w / cols

    # draw vertical lines
    for x in np.linspace(start=dx, stop=w - dx, num=cols - 1):
        x = int(round(x))
        cv.line(img, (x, 0), (x, h), color=color, thickness=thickness)

    # draw horizontal lines
    for y in np.linspace(start=dy, stop=h - dy, num=rows - 1):
        y = int(round(y))
        cv.line(img, (0, y), (w, y), color=color, thickness=thickness)

    return img


def my_estimatePoseSingleMarkers(corners, marker_size, mtx, distortion):
    '''
    This will estimate the rvec and tvec for each of the marker corners detected by:
       corners, ids, rejectedImgPoints = detector.detectMarkers(image)
    corners - is an array of detected corners for each detected marker in the image
    marker_size - is the size of the detected markers
    mtx - is the camera matrix
    distortion - is the camera distortion matrix
    RETURN list of rvecs, tvecs, and trash (so that it corresponds to the old estimatePoseSingleMarkers())
    '''
    marker_points = np.array([[-marker_size / 2, marker_size / 2, 0],
                              [marker_size / 2, marker_size / 2, 0],
                              [marker_size / 2, -marker_size / 2, 0],
                              [-marker_size / 2, -marker_size / 2, 0]], dtype=np.float32)
    trash = []
    rvecs = []
    tvecs = []
    for c in corners:
        nada, R, t = cv.solvePnP(marker_points, c, mtx, distortion, False, cv.SOLVEPNP_IPPE_SQUARE)
        rvecs.append(R)
        tvecs.append(t)
        trash.append(nada)
    return rvecs, tvecs, trash

def move_robotic_arm(x,y):
    pidevice.MOV(x, y)
    # pass

def from_camera_to_robot(x, y):
    # TODO: translate from camera pixels to robot coordinate system 
    return x,y


def drawfunction(event, x, y, flags, param):
    if event == cv.EVENT_LBUTTONDOWN:
        print(f"MOUSE PRESSED: {x, y}")
        x_new, y_new = from_camera_to_robot(x, y)
        move_robotic_arm(x_new, y_new)
    #    cv.circle(frame,(x,y),10,(255,255,255),-1)


cv.namedWindow('frame')
cv.setMouseCallback('frame', drawfunction)

numberOfImagesToGrab = 100000
camera.PixelFormat = "BGR8"
camera.StartGrabbingMax(numberOfImagesToGrab)

while camera.IsGrabbing():
    frame_init = camera.RetrieveResult(300, pylon.TimeoutHandling_ThrowException)

    if frame_init.GrabSucceeded():
        # Access the image data.
        frame = frame_init.Array
        frame2 = Image.fromarray(frame)
        frame2 = frame2.resize((600,600))
        frame = np.array(frame2)

        gray_frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        marker_corners, marker_IDs, reject = detector.detectMarkers(gray_frame)
        
        if marker_corners:
            rVec, tVec, _ = my_estimatePoseSingleMarkers(
                marker_corners, MARKER_SIZE, cam_mat, dist_coef
            )
            
            total_markers = range(0, marker_IDs.size)
            for ids, corners, i in zip(marker_IDs, marker_corners, total_markers):
                cv.polylines(
                    frame, [corners.astype(np.int32)], True, (0, 255, 255), 4, cv.LINE_AA
                )
                corners = corners.reshape(4, 2)
                corners = corners.astype(int)
                top_right = corners[0].ravel()
                top_left = corners[1].ravel()
                bottom_right = corners[2].ravel()
                bottom_left = corners[3].ravel()

                if ids[0] == 0:
                    x = bottom_right[0]
                    y = bottom_right[1]
                    center_point_id0 = (x, y)
                if ids[0] == 1:
                    x = top_right[0]
                    y = top_right[1]
                    center_point_id1 = (x, y)

                color_rec = (255, 0, 0)
                rectangle = cv.rectangle(frame, center_point_id0, center_point_id1, color_rec, thickness=2)

                # Calculating the distance
                distance = np.sqrt(
                    tVec[i][2] ** 2 + tVec[i][0] ** 2 + tVec[i][1] ** 2
                )
                # Draw the pose of the marker
                point = cv.drawFrameAxes(frame, cam_mat, dist_coef, rVec[i], tVec[i], 4, 4)
                cv.putText(
                    frame,
                    f"id: {ids[0]} Dist: {np.round(distance, 2)}",
                    top_right,
                    cv.FONT_HERSHEY_PLAIN,
                    1.3,
                    (0, 0, 255),
                    2,
                    cv.LINE_AA,
                )
                cv.putText(
                    frame,
                    f"x:{np.round(tVec[i][0], 1)} y: {np.round(tVec[i][1], 1)}, z:{np.round(tVec[i][2], 1)} ",
                    bottom_right,
                    cv.FONT_HERSHEY_PLAIN,
                    1.0,
                    (0, 0, 255),
                    2,
                    cv.LINE_AA,
                )
                # Custom (rgb) grid color
                grid_color = [0, 0, 0]
                frame = draw_grid(img=frame, grid_shape=(50, 50), color=grid_color)

        cv.imshow("frame", frame)
        key = cv.waitKey(1)
        if key == ord("q"):
            break
    frame_init.Release()
camera.Close()
cv.destroyAllWindows()
